require 'sinatra'
require 'json'
require 'rest-client'

get '/count' do
  halt 422, "Must include a url!" unless params['url']
  parsed_body = parsed_response_for_url(params['url'])

  word_count_response(parsed_body)
end

private

def parsed_response_for_url(url)
  response = RestClient.get(
    "https://mercury.postlight.com/parser?url=#{url}",
    headers={"x-api-key" => ENV["API_KEY"]},
  )

  JSON.parse(response.body)
end

def word_count_response(parsed_body)
  content_type :json
  status 200

  {:word_count => parsed_body["word_count"]}.to_json
end
